<?php
/**
 * @file
 *
 * Gateway navigation bar template.
 *
 * Available variables:
 * - $menu: A list of menu links.
 * - $links: Rendered menu links.
 *
 * @var $menu array
 * @var $links string
 */
?>
<div id="acton-gateway-nav-wrap">
  <div id="gw-nav" class="clearfix">
    <div id="acton-gateway-navigation-container">
      <h2 class="element-invisible">Gateway navigation</h2>
      <div id="acton-gateway-navigation" class="hide-rsp">
        <?php print $links; ?>
      </div>
      <div id="acton-gateway-navigation-responsive" class="show-rsp">
        <?php print $links; ?>
      </div>
    </div>
  </div>
</div>
