<?php
/**
 * @file
 * Theme definition and implementation.
 */

/**
 * Defines theme hooks.
 */
function _acton_gateway_theme() {
  $base = array(
    'file' => 'acton_gateway.theme.inc',
    'path' => drupal_get_path('module', 'acton_gateway') . '/theme',
  );
  return array(
    'acton_gateway_navigation' => array(
      'variables' => array(
        'menu' => array(),
      ),
      'template' => 'acton-gateway-navigation',
    ) + $base,
  );
}

/**
 * Prepares horizontal navigation display.
 */
function template_preprocess_acton_gateway_navigation(&$variables) {
  drupal_add_css(drupal_get_path('module', 'acton_gateway') . '/css/acton_gateway.menu.css');
  drupal_add_js(drupal_get_path('module', 'acton_gateway') . '/js/acton_gateway.menu.js');
  $variables['links'] = theme('links__system_main_menu', array('links' => $variables['menu'], 'attributes' => array('class' => array('menu'))));
}
