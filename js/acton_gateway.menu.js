(function ($) {

Drupal.behaviors.acton_gateway_responsive_menu = {
  attach: function (context, settings) {
    // Transform responsive menu into jump menu.
    var jumpMenu = $('<select></select>');
    $('#acton-gateway-navigation-responsive ul', context)
      .find('li a').each(function () {
        var option = $('<option></option>').html('&raquo; ' + $(this).html()).val($(this).attr('href'));
        if ($(this).is('.active, .active-trail')) {
          option.attr('selected', 'selected');
        }
        jumpMenu.append(option);
      }).end()
      .replaceWith(jumpMenu);
    jumpMenu.change(function () {
      var href;
      if (href = $('option:selected', this).val()) {
        window.location.href = href;
      }
    });
  }
};

})(jQuery);